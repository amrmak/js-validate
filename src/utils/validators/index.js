import * as RULES from './rules';
import {
  isEmpty,
  compareLength,
  isBigger,
  isValidEmail,
  isValidNumber,
} from '../helpers';

/**
 * @description check if form input is valid
 * @param {Object} input form input DOM Node
 * @param {Object} rule validation rule Object { [rule]: any, message: String}
 * @returns {Object} { isValid: Boolean, message: String }
 */
const isInputValid = (input, rule) => {
  if (rule[RULES.REQUIRED]) {
    return {
      isValid: isEmpty(input.value),
      message: rule.message || 'This field is required',
    };
  }

  if (rule[RULES.LENGTH]) {
    return {
      isValid: compareLength(input.value, rule[RULES.LENGTH]),
      message:
        rule.message ||
        `This field length should be exactly ${rule[RULES.LENGTH]}`,
    };
  }

  if (rule[RULES.MAX]) {
    return {
      isValid: !isBigger(input.value, rule[RULES.MAX]),
      message:
        rule.message ||
        `This field's value shuold not exceed ${rule[RULES.MAX]}`,
    };
  }

  if (rule[RULES.MIN]) {
    return {
      isValid: isBigger(input.value, rule[RULES.MIN]),
      message:
        rule.message ||
        `This field's value shuold be at least ${rule[RULES.MIN]}`,
    };
  }

  if (rule[RULES.TYPE]) {
    switch (rule[RULES.TYPE]) {
      case RULES.TYPES.EMAIL:
        return {
          isValid: isValidEmail(input.value),
          message: rule.message || 'Invalid email',
        };
      case RULES.TYPES.NUMBER:
        return {
          isValid: isValidNumber(input.value),
          message: rule.message || 'This field should be of type Number',
        };

      default:
        break;
    }
  }

  return true;
};

export default isInputValid;
