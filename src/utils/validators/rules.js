export const REQUIRED = 'required';
export const LENGTH = 'length';
export const MAX = 'max';
export const MIN = 'min';
export const TYPE = 'type';
export const TYPES = {
  EMAIL: 'email',
  NUMBER: 'number',
}