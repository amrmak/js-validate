export const isEmpty = input => !!input;

/**
 *
 * @param {String} source form input value
 * @param {Number, String} target length to be compared to
 */
export const compareLength = (source, target) => {
  if (typeof source !== 'string') return false;
  if (typeof target === 'number' && typeof target !== 'string') return false;

  let targetNum = parseInt(target, 10);
  return source.length === targetNum;
};

/**
 *
 * @param {Number, String} source form input value
 * @param {Number, String} target length to be compared to
 */
export const isBigger = (source, target) => {
  if (typeof source !== 'number' && typeof source !== 'string') return false;
  if (typeof target !== 'number' && typeof target !== 'string') return false;

  let sourceNum = parseInt(source, 10) || sourceNum.length;
  let targetNum = parseInt(target, 10);
  return sourceNum > targetNum && sourceNum !== targetNum;
};

export const isValidEmail = email => {
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
};

export const isValidNumber = number => {
  return typeof number === 'number';
};
