import isInputValid from './src/utils/validators';

window.JsValidate = class JsValidate {
  constructor(formIdentifier) {
    const form = document.forms[formIdentifier];
    if (!form)
      throw Error(`Cannot find form with an iddentifier: ${formIdentifier}`);

    this.formElements = form.elements;
    this.errors = {};
    this.rules = {};
  }

  /**
   * @description this function add an error message to the class's error object
   * with form input name as it's key
   * @param {String} inputName form input name
   * @param {String} message validation rule error message
   */
  addErrorMessage(inputName, message = 'Invalid Input') {
    if (!this.errors[inputName]) {
      this.errors[inputName] = [message];
      return;
    }
    this.errors[inputName].push(message);
  }

  /**
   * @description this function add a rule to the class's rules array
   * with form input name as it's key
   * @param {String} inputName form input name
   * @param {Object} rule validation rule object
   */
  addRule(inputName, rule) {
    if (!this.rules[inputName]) {
      this.rules[inputName] = [rule];
      return;
    }
    this.rules[inputName].push(rule);
  }

  /**
   * @description add a validation rule for a form input
   * @param {String} inputName
   * @param {Object} options
   */
  addValidation(inputName, options = {}) {
    const el = this.formElements[inputName];
    if (!el)
      throw Error(`Cannot find form input with the name of: ${inputName}`);

    const { rules } = options;
    if (!rules || !rules.length)
      throw Error(`Validation rules for input "${inputName}" are missing`);

    rules.forEach(rule => {
      this.addRule(inputName, rule);
    });
  }

  validate() {
    Object.keys(this.rules).forEach(key => {
      let rules = this.rules[key];
      rules.forEach(rule => {
        let { isValid, message } = isInputValid(this.formElements[key], rule);
        if (!isValid) this.addErrorMessage(key, message);
      });
    });

    return this.errors
  }
}
